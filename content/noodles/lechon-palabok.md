---
title: "Lechon Palabok @ 125 pesos"
# date: 2022-04-17T11:22:16+06:00
description : "Palabok is a Filipino rice noodle dish that we topped with our lechon"
type: "menu"
draft: true
cost: "Php 125"
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
images:
  - image: "noimg.png"
  # - image: "n/palabok.jpg"
---
