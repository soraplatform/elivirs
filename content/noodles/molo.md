---
title: "Molo Soup @ 100 pesos"
# date: 2022-04-17T11:22:16+06:00
description : "Our Molo Soup is fresh and meaty"
type: "menu"
cost: "Php 100"
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
# first image will be shown in the product page
images:
  # - image: "noimg.png"
  - image: "s/molo.jpg"
---
