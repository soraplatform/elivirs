---
title: "Pancit Bihon"
# date: 2022-04-17T11:22:16+06:00
description : "Pancit Bihon uses rice noodles instead of wheat"
type: "menu"
cost: "Php 125"
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
# first image will be shown in the product page
images:
  # - image: "noimg.png"
  - image: "n/bihon.jpg"
---
