---
title: "Lechon Sotanghon @ 125 pesos"
# date: 2022-04-17T11:22:16+06:00
description : "Elivir's Sotanghon is also a rice noodle dish topped with Lechon bits"
type: "menu"
cost: "Php 125"
draft: true
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
images:
  - image: "noimg.png"
  # - image: "n/sotanghon.jpg"
---
