---
title: "Buffalo Wings @ 50 pesos"
# date: 2022-04-17T11:22:16+06:00
description : "Chicharon Bulaklak is fried Pork Skin"
type: "menu"
cost: "Php 50"
draft: true
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
images:
  - image: "m/chicharon.jpg"
  # - image: "noimg.png"
---

