---
title: "Bangsilog @ 120 pesos"
# date: 2022-04-17T11:22:16+06:00
description : "Bangsilog is a Filipino dish that combines bangus, sinangag (fried rice), and egg."
type: "menu"
cost: "Php 120"
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
images:
  - image: "b/bang.jpg"
---

