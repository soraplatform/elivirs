---
title: "Spamsilog @ 90 pesos"
# date: 2022-04-17T11:22:16+06:00
description : "Spamsilog is a Filipino dish that combines meatloaf, sinangag (fried rice), and egg."
type: "menu"
cost: "Php 90"
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
# first image will be shown in the product page
images:
  # - image: "noimg.png"
  - image: "b/spam.jpg"
---
