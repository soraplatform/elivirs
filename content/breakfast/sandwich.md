---
title: "Flavored Sandwiches @ 25 pesos"
# date: 2022-04-17T11:22:16+06:00
description : "Our sandwiches come in grape jelly, peanut butter, and strawberry flavors"
type: "menu"
cost: "Php 25"
draft: true
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
images:
  - image: "b/strawberry.jpg"
  - image: "b/peanut.jpg"  
---
