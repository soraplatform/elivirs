---
title: "Tapsilog @ 130 pesos"
# date: 2022-04-17T11:22:16+06:00
description : "Tapsilog is a Filipino dish that combines beef tapa, sinangag (fried rice), and egg."
type: "menu"
cost: "Php 130"
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
draft: true
# first image will be shown in the product page
images:
  - image: "b/tapsi.jpg"
---
