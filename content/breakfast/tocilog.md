---
title: "Tocilog @ 90 pesos"
# date: 2022-04-17T11:22:16+06:00
description : "Our Tocilog is nearly as popular as our Tapsilog"
type: "menu"
cost: "Php 90"
# costdef: "1 point = 1 value of 1 kilo rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
images:
  # - image: "noimg.png"
  - image: "b/toci.jpg"
---

