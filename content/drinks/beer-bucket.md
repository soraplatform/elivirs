---
title: "Beer Bucket @ 475 pesos"
# date: 2022-04-17T11:22:16+06:00
description : "We have San Miguel Beer Bucket either Pale Pilsen or Light"
type: "menu"
cost: "Php 475"
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
images:
  # - image: "noimg.png"
  - image: "d/bucket.jpg"
---
