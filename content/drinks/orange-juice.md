---
title: "Orange Juice @ 60 pesos"
# date: 2022-04-17T11:22:16+06:00
description: "Our orange Juice is available in season only"
type: "menu"
cost: "Php 60"
draft: true
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
images:
  - image: "d/oj.jpg"
---
