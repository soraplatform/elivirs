---
title: "Milk @ 40 pesos"
# date: 2022-04-17T11:22:16+06:00
description : "Milk"
type: "menu"
cost: "Php 40"
draft: true
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
images:
  - image: "d/milk.jpg"
---
