---
title: "Hot Chocolate @ 65 pesos"
# date: 2022-04-17T11:22:16+06:00
description : "Hot Chocolate"
type: "menu"
cost: "Php 65"
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
draft: true
images:
  - image: "noimg.png"
  # - image: "d/choco.jpg"
---
