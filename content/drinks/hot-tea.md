---
title: "Hot Lipton Tea @ 20 pesos"
# date: 2022-04-17T11:22:16+06:00
description : "Hot tea is useful to stimulate the digestive system before a meal."
type: "menu"
cost: "Php 20"
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
images:
  - image: "d/hottea.jpg"
---
