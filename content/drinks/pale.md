---
title: "San Miguel Pale Pilsen @ 85 pesos"
# date: 2022-04-17T11:22:16+06:00
description : "Best enjoyed with your friends"
type: "menu"
cost: "Php 85"
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
images:
  # - image: "noimg.png"
  - image: "d/pale.png"
---
