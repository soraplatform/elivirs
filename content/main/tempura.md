---
title: "Fish and Shrimp Tempura @ 220 pesos"
# date: 2022-04-17T11:22:16+06:00
description : "This dish is available when seafood is available"
type: "menu"
cost: "Php 220"
draft: true
# costdef: "1 point = 1 value of 1 kilo rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
images:
  - image: "m/tempura2.jpg"
  # - image: "noimg.png"
---
