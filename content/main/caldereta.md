---
title: "Caldereta Ribs in Gata (Coconut Milk) @ 200 pesos"
# date: 2022-04-17T11:22:16+06:00
description : "Caldereta is a traditional Filipino dish in coconut milk"
type: "menu"
cost: "Php 200"
draft: true
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
images:
  - image: "m/caldereta.jpg"
---

