---
title: "Chicken in Pineapple (Pininyahang Manok) @ 150 pesos"
# eef Steak with Mushroom
# date: 2022-04-17T11:22:16+06:00
description : "Chicken in Pineapple (Pininyahang Manok) is chicken with pineapple, carrots, potatoes, and bell peppers "
type: "menu"
cost: "Php 150"
draft: true
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
images:
  - image: "m/chickenpineapple.jpg"
  - image: "m/chickenpineapple2.jpg"  
---

