---
title: "Lechon Paksiw de Cebu @ 150 pesos"
# date: 2022-04-17T11:22:16+06:00
description : "Lechon Paksiw de Cebu is among our specialties"
type: "menu"
cost: "Php 150 @ 125 grams"
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
images:
  - image: "m/paksiw2.jpg"
---

