---
title: "Calamari @ 225 pesos"
# date: 2022-04-17T11:22:16+06:00
description : "Calamari is squid deep fried in batter and served with a dip"
type: "menu"
cost: "Php 225"
draft: true
costdef: "Approximately 100 grams"
# 1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)
cta: "Contact us to Inquire"
images:
  - image: "m/calamari.jpg"    
---

