---
title: "Fish Escabeche Pampano @ 350 pesos"
# date: 2022-04-17T11:22:16+06:00
description : "Fish Escabeche Pampano is a traditional Filipino seafood dish. This is available only when stocks last, so please contact us in advance"
type: "menu"
cost: "Php 350"
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
images:
  - image: "m/fish.jpg"
---
 