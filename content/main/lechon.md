---
title: "Lechon de Cebu"
# date: 2022-04-17T11:22:16+06:00
description : "This is our main specialty. For orders, please contact us a day in advance"
type: "menu"
cost: "Php 150 for 125 grams; Php 300 for 250 grams; Php 550 for 500 grams; Php 1,000 for 1 kg"
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
images:
  - image: "m/lechon.jpg"
  - image: "m/500.jpg"
  - image: "m/250.jpg"
  - image: "m/125.jpg"      
  # - image: "noimg.png"
---

