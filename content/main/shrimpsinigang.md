---
title: "Shrimp Sinigang (Sinigang na Hipon) @ 350 pesos"
# date: 2022-04-17T11:22:16+06:00
description : "Shrimp Sinigang (Sinigang na Hipon) is a famous Filipino soup that is ranked highly around the world"
type: "menu"
cost: "Php 350"
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
images:
  - image: "m/shrimpsinigang.jpg"
---

