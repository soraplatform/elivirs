---
title: "Bistek Tagalog @ 150 pesos"
# eef Steak with Mushroom
# date: 2022-04-17T11:22:16+06:00
description : "Bistek Tagalog is beef with onions"
type: "menu"
draft: true
cost: "Php 150"
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
images:
  - image: "m/beefsteak.jpg"
  - image: "m/beefsteak2.jpg"  
---
