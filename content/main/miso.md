---
title: "Sinigang sa Miso @ 250 pesos"
# date: 2022-04-17T11:22:16+06:00
description : "Sinigang sa Miso"
type: "menu"
cost: "Php 250"
draft: true
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
# first image will be shown in the product page
images:
  - image: "m/beef.jpg"
---
