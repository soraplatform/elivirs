---
title: "Pork Sinigang (Sinigang na Baboy) @ 250 pesos"
# date: 2022-04-17T11:22:16+06:00
description : "Pork Sinigang  is the classic Filipino sinigang"
type: "menu"
cost: "Php 250"
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
images:
  - image: "m/porksinigang.jpg"
  # - image: "noimg.png"
---

