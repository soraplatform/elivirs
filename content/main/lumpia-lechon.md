---
title: "Lumpiang Shanghai Lechon 4 Pieces @ 75 pesos"
# date: 2022-04-17T11:22:16+06:00
description : "Lumpiang Shanghai Lechon is our lechon wrapped in rice paper"
type: "menu"
cost: "Php 75"
draft: true
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
images:
  - image: "m/lumpia.jpg"
---

