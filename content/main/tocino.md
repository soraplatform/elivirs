---
title: "Pork Tocino @ 100 pesos"
# date: 2022-04-17T11:22:16+06:00
description : "Our Pork Tocino also comes from our pig supplier who uses environmentally-conscious feeds"
type: "menu"
cost: "Php 100"
draft: true
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
images:
  - image: "m/tocino.jpg"
  # - image: "noimg.png"
---
