---
title: "Buttered Shrimp @ 285 pesos"
# date: 2022-04-17T11:22:16+06:00
description : "Buttered Shrimp is available only when shrimp is in stock so please contact us in advance"
type: "menu"
cost: "Php 285"
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
images:
  - image: "m/shrimp.jpg"
  #- image: "noimg.png"
---

