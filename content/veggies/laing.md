---
title: "Special Laing @ 120 pesos"
# date: 2022-04-17T11:22:16+06:00
description : "Laing is a common leafy vegetable in the Philippines. It is cooked in coconut oil as a healthy dish."
type: "menu"
cost: "Php 120"
draft: true
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
# first image will be shown in the product page
images:
  # - image: "noimg.png"
  - image: "v/laing.jpg"
---
