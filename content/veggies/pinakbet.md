---
title: "Pinakbet @ 100 pesos"
# date: 2022-04-17T11:22:16+06:00
description : "Pinakbet is a traditional Filipino vegetable dish"
type: "menu"
cost: "Php 100"
draft: true
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
images:
  # - image: "noimg.png"
  - image: "v/pinakbet.jpg"
---
