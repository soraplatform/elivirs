---
title: "Togue with Tokwa @ 150 pesos"
# date: 2022-04-17T11:22:16+06:00
description : "Togue with Tokwa is our version of tokwa't baboy."
type: "menu"
cost: "Php 150"
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
# first image will be shown in the product page
images:
  # - image: "noimg.png"
  - image: "v/tokwa.jpg"
---
