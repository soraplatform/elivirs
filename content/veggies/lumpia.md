---
title: "Lumpia Platter 20 Pieces @ 220 pesos"
# date: 2022-04-17T11:22:16+06:00
description : "Lumpiang Gulay (Vegetable) is vegetable wrapped in rice paper"
type: "menu"
cost: "Php 11 per piece"
# draft: true
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
images:
  - image: "v/lumpiaplatter.jpg"
  # - image: "noimg.png"
---

