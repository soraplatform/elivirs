---
title: "Chopsuey @ 195 pesos"
# date: 2022-04-17T11:22:16+06:00
description : "Chopsuey is a foreign dish that has made its way into Filipino culture."
type: "menu"
cost: "Php 195"
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
images:
  # - image: "noimg.png"
  - image: "v/chopsuey.jpg"
---
