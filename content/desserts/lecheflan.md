---
title: "Leche Flan @ 150 pesos"
# date: 2022-04-17T11:22:16+06:00
description : "Leche Flan is made from eggs and is sweet"
type: "menu"
cost: "Php 150"
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
images:
  # - image: "noimg.png"  
  - image: "des/leche.jpg"
---

