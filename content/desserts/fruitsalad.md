---
title: "Fruit Salad @ 100 pesos"
# date: 2022-04-17T11:22:16+06:00
description : "Filpino Fruit Salad uses cream to be the ideal healthy dessert"
type: "menu"
cost: "Php 100"
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
images:
  # - image: "noimg.png"  
  - image: "des/fruitsalad.jpg"
---
