---
title: "Maja Blanca @ 125 pesos"
# date: 2022-04-17T11:22:16+06:00
description : "Maja Blanca is a classic Filipino dessert"
type: "menu"
cost: "Php 125"
draft: true
# costdef: "per gram"
cta: "Contact us to Inquire"
images:
  # - image: "noimg.png"  
  - image: "des/maha.jpg"
---
