---
title: "Arroz Caldo with Chicken @ 100 pesos"
# date: 2022-04-17T11:22:16+06:00
description : "Our Arroz Caldo comes with hard-boiled egg, chopped scallions, toasted garlic, and calamansi and available at our Berkeley Square location."
type: "menu"
cost: "Php 100"
draft: true
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
images:
  # - image: "noimg.png"
  - image: "s/arroz.jpg"
---


Arroz Caldo is Filipino-style rice congee or "lugaw" made of chicken and rice and flavored with fresh ginger, garlic, onions, and fish sauce.

It means “brothy rice” in Spanish and has Chinese origins. 

We highly recommend it during cool, rainy days!



<!-- commonly eaten for breakfast, but it’s also commonly enjoyed as a midday snack or light dinner meal.


 tasty and filling, ginger-flavored broth, and various toppings

Arroz Caldo or aroskaldo is a Filipino lugaw  Although its name is derived from

It comes  the Spanish arroz caldoso 

 

 the rice gruel is more of a local adaptation of the congee introduced in the Philippines by Chinese immigrants. -->

