---
title: "Sinigang na Lechon @ 200 pesos"
# date: 2022-04-17T11:22:16+06:00
description : "Put Lechon in Sinigang and you get our famous Sinigang na Lechon"
type: "menu"
cost: "Php 200"
draft: true
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Message us at FB or leave a message below"
images:
  # - image: "noimg.png"
  - image: "s/siniganglechon.jpg"
---

