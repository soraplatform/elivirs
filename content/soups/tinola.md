---
title: "Chicken Tinola @ 200 pesos"
# date: 2022-04-17T11:22:16+06:00
description : "Our warm Chicken Tinola is good for 2 people"
type: "menu"
cost: "Php 200"
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
images: 
  # - image: "noimg.png"
  - image: "s/tinola.jpg"
---
