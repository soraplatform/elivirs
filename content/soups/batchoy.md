---
title: "Batchoy Tagalog @ 140 pesos"
# date: 2022-04-17T11:22:16+06:00
description : "Our Batchoy Tagalog has less fat and more value for money"
type: "menu"
cost: "Php 140"
draft: true
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
images:
  - image: "s/batchoy.jpg"
---

