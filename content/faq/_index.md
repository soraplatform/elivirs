---
title: "FAQ"
# description : "FAQ for Elivir's Lechon"
video:
  videoThumb : "/graphics/ourfounder.jpg"
  videoURL : "https://www.youtube.com/embed/rMNAF1AzzZY"
---


## What is the inspiration for Elivir's Lechon?

Elivir's was created by Virgil Obsina after a long career in restaurants in Florida and Califoria in the United States. 

It focuses on making world-class quality Filipino food that is affordable for everyone.  


## How do I get to Elivir's ?

By public transportation: 

- Take the MRT or EDSA Carousel to Quezon Avenue 
<!-- - From GMA-Kamuning, walk to East Avenue and take a LITEX jeepney to Tandang Sora -->
- From Quezon Avenue, take a LITEX jeepney to Tandang Sora
- Alight at Berkeley Square just before Tandang Sora

<!-- By EDSA Carousel bus:

- Take the bus to Quezon Avenue Eton
- Take a LITEX jeepney from Eton
 --> 

## Do you deliver?

We will deliver soon!


<!-- ## What are points and how do I pay in points?

Points are our version of barter. We give a service and you give us a service or product of the same value. That value is set in points. For example, we can give a yoga class for 40 points. You can render a service to us or give us a product of the same amount. This is useful in a high inflation scenario like what we have today.  

1 point = 1 kilo of NFA rice which is 35 pesos before rice tariffication was established. 


### Can I pay in cash? 

Yes! We accept both cash and points.

Just convert 1 point into 35 pesos. Therefore, 40 points = 1,400 pesos. If NFA rice goes up to 40 pesos per kilo, then 40 points becomes 1,600 pesos.   -->


## Do you have any advocacies?

Yes! We advocate:
- Filipino food and culture
- Food Safety and Nutrition
- Environmentalism
- The development of local agriculture over foreign imports
- Increasing employment
